import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'

import Home from './pages/Home'

import Login from './pages/Login'
import PrivateRoute from './privateRoute'
import Layout from './components/layout/Layout'

function App() {
  return (
    <div className='App'>
      <Router>
        <Layout>
          <Switch>
            <Route path='/Login'>
              <Login />
            </Route>

            <PrivateRoute exact path='/' component={Home} />
          </Switch>
        </Layout>
      </Router>
    </div>
  )
}

export default App
