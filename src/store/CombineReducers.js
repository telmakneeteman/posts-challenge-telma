import { combineReducers } from 'redux'
import PostReducer from './posts/PostReducer'
import { authReducer } from './auth/authReducer'

export default combineReducers({
  postsRootReducer: PostReducer,
  authRootReducer: authReducer,
})
