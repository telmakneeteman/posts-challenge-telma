import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import rootReducer from './CombineReducers'
import { loadState, saveState } from './LocalStorage'

const persistedStore = loadState()

const middleware = [thunk]

const store = createStore(
  rootReducer,
  persistedStore,
  composeWithDevTools(applyMiddleware(...middleware))
)
store.subscribe(() => {
  saveState(store.getState())
})

export default store
