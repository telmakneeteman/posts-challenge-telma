import * as actionTypes from './PostConstants'

const initialState = {
  posts: [],
  loading: true,
}

const PostReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_POSTS:
      return {
        ...state,
        posts: action.payload,
        loading: false,
      }

    case actionTypes.ADD_POST:
      return {
        ...state,
        posts: [...state.posts, action.payload],
      }

    case actionTypes.UPDATE_POST:
      return {
        ...state,
        posts: [
          state.posts.map((post) =>
            post.id === action.payload.id ? action.payload : post
          ),
        ],
      }

    case actionTypes.DELETE_POST:
      return {
        ...state,
        posts: [...state.posts.filter((id) => id !== action.payload)],
      }
    default:
      return state
  }
}

export default PostReducer
