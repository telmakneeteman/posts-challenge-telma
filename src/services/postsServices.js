import axios from 'axios'
import { baseURL } from '../api/api'

export const getAll = () => {
  return axios.get(baseURL + '/posts')
}
export const get = (id) => {
  return axios.get(baseURL + `/posts/${id}`)
}
export const create = (data) => {
  return axios.post(baseURL + '/posts', data)
}
export const update = (data) => {
  return axios.put(baseURL + `/posts/${data.id}`, data)
}
export const remove = (id) => {
  return axios.delete(baseURL + `/posts/${id}`)
}
