import * as actionTypes from './PostConstants'
import * as serviceTypes from '../../services/postsServices'

export const getPosts = () => async (dispatch) => {
  try {
    const res = await serviceTypes.getAll()
    dispatch({
      type: actionTypes.GET_POSTS,
      payload: res.data,
    })
  } catch (err) {
    dispatch({
      type: actionTypes.ERR_POSTS,
      payload: console.log(err),
    })
  }
}

export const createPost = (title, body, id, userId) => async (dispatch) => {
  try {
    const res = await serviceTypes.create({ title, body, id, userId })

    dispatch({
      type: actionTypes.ADD_POST,
      payload: { title, body, id, userId },
    })
    return Promise.resolve(res.data)
  } catch (err) {
    return Promise.reject(err)
  }
}

export const updatePost =
  ({ title, description, id }) =>
  async (dispatch) => {
    try {
      const res = await serviceTypes.update({ title, description, id })
      dispatch({
        type: actionTypes.UPDATE_POST,
        payload: { title, description, id },
      })
      return Promise.resolve(res.data)
    } catch (err) {
      return Promise.reject(err)
    }
  }

export const deletePost = (id) => async (dispatch) => {
  try {
    const res = await serviceTypes.remove(id)
    dispatch({
      type: actionTypes.DELETE_POST,
      payload: id,
    })
    return Promise.resolve(res)
  } catch (err) {
    return Promise.reject(err)
  }
}
