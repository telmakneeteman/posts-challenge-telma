import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import * as serviceTypes from '../../../services/postsServices'
import { deletePost } from '../../../store/posts/PostAction'

import ModalForm from '../../modal/ModalForm'
import { FaTrashAlt, FaEye, FaEdit } from 'react-icons/fa'
import './post.css'

const Post = (props) => {
  const [show, setShow] = useState(false)
  const [edit, setEdit] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  const forEdit = () => setEdit(true)
  const forDetails = () => setEdit(false)

  const dispatch = useDispatch()

  const removePost = (id) => {
    dispatch(deletePost(id))
  }

  return (
    <div>
      <ModalForm
        show={show}
        onHide={handleClose}
        title={props.title}
        description={props.body}
        edit={edit}
      />
      <div className='line-color' />
      <div className='widget-content p-0'>
        <div className='widget-content-wrapper'>
          <div className='widget-content-left'>
            <div className='widget-heading'>{props.title}</div>
          </div>
          <div className='widget-content-right'>
            <button
              onClick={() => {
                handleShow()
                forDetails()
              }}
              className='border-0 btn-transition btn btn-outline-info btn-see-more'
            >
              <FaEye />
            </button>
            <button
              onClick={() => {
                handleShow()
                forEdit()
              }}
              className='border-0 btn-transition btn btn-outline-secondary btn-edit'
            >
              <FaEdit />
            </button>
            <button className='border-0 btn-transition btn btn-outline-danger btn-delete'>
              <FaTrashAlt
                onClick={() => {
                  removePost(props.id)
                }}
              />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Post
